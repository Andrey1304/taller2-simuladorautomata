// Definimos el alfabeto
const alfabeto = ["0", "1"];
// Definimos la tabla de transiciones
const tablaTransiciones = [
  [1, 2],    // q0
  [3, 2],    // q1
  [1, 4],    // q2
  [3, 2],    // q3
  [1, 4]     // q4
];
// Definimos el estado inicial y los estados de aceptación
const estadoInicial = 0;
const estadosAceptacion = [4];
// Función para validar una cadena
function validarCadena(cadena) {
  let estadoActual = estadoInicial;
   for (let i = 0; i < cadena.length; i++) {
let simbolo = cadena[i];
let indiceSimbolo = alfabeto.indexOf(simbolo);
if (indiceSimbolo === -1) {
// Si el símbolo no pertenece al alfabeto, la cadena es inválida
return false;
}
estadoActual = tablaTransiciones[estadoActual][indiceSimbolo];
}
return estadosAceptacion.includes(estadoActual);
}
// Función para validar la cadena ingresada
function validar() {
let cadena = document.getElementById("cadena").value;
let mensaje = document.getElementById("mensaje");
if (validarCadena(cadena)) {
mensaje.innerText = "La cadena es válida.";
mensaje.className = "valido";
} else {
mensaje.innerText = "La cadena es inválida.";
mensaje.className = "invalido";
}
}
// Función para limpiar la entrada y el mensaje de validación
function limpiar() {
document.getElementById("cadena").value = "";
document.getElementById("mensaje").innerText = "";
document.getElementById("mensaje").className = "";
}